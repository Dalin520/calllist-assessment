import { createStore,compose,applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {callCombineReducer} from '../reducers/callCombineReducer';
const middleWare=[thunk];
const initState={};
export const CallCentralStore =createStore(callCombineReducer,initState,compose(applyMiddleware(...middleWare)))
