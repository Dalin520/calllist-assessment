import React from 'react';
import { Text, View ,Image} from 'react-native';
import {Router,Scene } from 'react-native-router-flux'
import Home from '../Home';
import ViewHistory from '../ViewHistory';
import Search from '../Search';

const Main =()=> {
    return(
        <Router>
            <Scene key="root">
                <Scene
                    key="home"
                    navigationBarStyle={{backgroundColor:'whitesmoke'}}
                    component={Home}
                    renderTitle={() => (
                        <View style={{justifyContent:'center',flex: 1,}}>
                          <Image source={require('../../res/img/call.png')} style={{ width: 35, height: 35 ,alignSelf:'center'}} />
                        </View>
                      )}
                    titleStyle={{ color: 'white', fontSize: 20, }}
                    initial
                />
                <Scene
                    key="history"
                    component={ViewHistory}
                    titleStyle={{ color: 'rgb(132,180,79)', fontSize: 20, }}
                    title="History"
                    hideNavBar
                    leftButtonIconStyle = {{ tintColor:'red'}}
                    back={true}
                />
                <Scene
                    key="search"
                    component={Search}
                    hideNavBar
                    back={true}
                />
            </Scene>
        </Router>
    );
}

export default Main;
