import React, { Component } from 'react';
import { 
    View ,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    Platform,
    ActivityIndicator,
    Alert,
    SafeAreaView,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import { 
    Content, 
    Card, 
    CardItem, 
    Text, 
    Right, 
    Body, 
} from 'native-base';
import {getInitailCall,deleteCall} from '../actions/callAction'
import { connect } from 'react-redux';
import {Actions} from 'react-native-router-flux'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            items:[]
        };
    }
   
    componentWillMount() {
        this.props.getInitailCall(); 
        this.setState({
            items:this.props.calls
        })
    }
   
    gotoHistory=(id,name)=> {
        const param = {
            id: id,
            name: name,
        }
        Actions.history(param)
    }
    handleOnChangeText=(text)=>{
        this.setState({text})
        this.props.getInitailCall(text)
    }

    _delete=(name,id,duration)=>{
        Alert.alert(
            `Delete "${name}" ?`,
            'Deleting this call will also delete its data.',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => this.props.deleteCall(id,duration)},
            ],
            {cancelable: false},
        );
    }

    render() {
        const { calls } = this.props
        const { text } = this.state;
        console.log('text==',text)
        return (
        <SafeAreaView style={styles.container}>
            {
                Platform.OS==='android'? 
                    <StatusBar backgroundColor="green" barStyle="light-content" />              
                :
                <StatusBar backgroundColor="green" barStyle="dark-content" /> 
            }
            <KeyboardAvoidingView style={styles.container}>
                <Content>
                    <View style={styles.searchBox}>
                        <Card>
                            <View style={styles.box}>
                                <Image source={require('../res/img/search.png')} style={styles.searchIcon} />
                                <Text
                                    style={{padding:15,flex:1,color:'gray'}}
                                    onPress={()=> Actions.search()}
                                >Search here...</Text>
                        </View>
                        </Card>
                    </View>

                    { calls? 
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {calls.map((item,index) => {
                            return (                    
                            <View key={index} style={{marginHorizontal:10}}>
                                <Card>
                                    <TouchableHighlight
                                        onPress={()=>this.gotoHistory(item.id,item.name)}
                                    >
                                        <CardItem>                 
                                        <Image source={item.img} style={{ width: 60, height: 60 }} resizeMode="cover" />
                                        <Body>
                                            <View style={styles.content}>
                                                <Text style={styles.name}>{item.name} </Text>
                                                <View style={styles.info}>
                                                    <Image source={require('../res/img/info.png')} style={{ width: 15, height: 15}} />
                                                    <Text style={styles.duration}>{item.duration}</Text>
                                                </View>
                                            </View>
                                        </Body>
                                        <Right>
                                            <TouchableOpacity
                                                onPress={()=>this._delete(item.name,item.id)}
                                            >
                                                <Image source={require('../res/img/delete.png')} style={{ width: 30, height: 30 }} />
                                            </TouchableOpacity>
                                        </Right>
                                        </CardItem>
                                    </TouchableHighlight>
                                </Card> 
                            </View>
                            )
                        })}
                    </ScrollView>
                        : <ActivityIndicator size="large" color="#0000ff" /> 
                    }
                </Content>
            </KeyboardAvoidingView>
        </SafeAreaView>
        );
    }
    }
const mapStateToProps = (state) => {
    return {
        calls: state.callList.calls,
    }
}
export default connect(mapStateToProps, {getInitailCall,deleteCall})(Home);

const styles = StyleSheet.create({
    name:{
        fontSize: 14,
    },
    info:{
        marginTop: 5,
        flex:1,
        flexDirection: 'row',
    },
    duration:{
        color:'gray',
        fontSize:12,
        marginLeft:5,
    },
    content:{
        marginLeft:15,
        marginTop:10,
        flex:1,
        justifyContent: 'center',
    },
    container:{
        flex:1,
        backgroundColor:'#eee',
        flexDirection: 'column',
    },
    input:{
        flex:1,
        backgroundColor:'white',
        color:'black',
        paddingHorizontal: 15,
    },
    searchBox:{
        flex:1,
        marginVertical: 5,
        marginTop:10,
        marginHorizontal:10,
    },
    box:{
        flex:1,
        flexDirection:'row-reverse',
    },
    searchIcon:{
        width:20,
        height:20,
        alignSelf: 'center',
        marginRight: 15,
        position:'absolute',
        top:15,
    }
})