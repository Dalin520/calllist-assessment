import React, { Component } from 'react';
import { 
    View ,
    FlatList,
    Image,
    TouchableHighlight,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    Platform,
    SafeAreaView,
    KeyboardAvoidingView,
} from 'react-native';
import { 
    Content, 
    Card, 
} from 'native-base';
import SearchableDropdown from 'react-native-searchable-dropdown'
import {getInitailCall,gotoHistory} from '../actions/callAction'
import { connect } from 'react-redux';
import {Actions} from 'react-native-router-flux'

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
        text: '',
        items:[]
    };
  }

  componentWillMount() {
    this.props.getInitailCall(); 
    this.setState({
        text: " ",
        items: this.props.calls
    })
  }
  componentWillUnmount(){
      this.setState({
          text: " "
      })
  }
  gotoHistory=(id,name)=> {
      this.setState({
          text: ' '
      })
      const param = {
          id: id,
          name: name,
      }
      Actions.history(param)
  }
  render() {
    const { calls } = this.props
    const { text } = this.state;
    return (
        <SafeAreaView style={styles.container}>
        {
            Platform.OS==='android'? 
                <StatusBar backgroundColor="green" barStyle="light-content" />              
            :
            <StatusBar backgroundColor="green" barStyle="dark-content" /> 
        }
        <KeyboardAvoidingView style={styles.container}>
            <Content>
                <View style={styles.searchBox}>
                    <Card>
                        <View style={styles.box}>
                            <SearchableDropdown
                                onItemSelect={item => {this.gotoHistory(item.id,item.name)}}
                                onTextChange={
                                    (text) => {
                                        this.setState({ text:text,items:calls})
                                    }
                                }
                                containerStyle={styles.containerStyle}
                                value={this.state.text}
                                textInputStyle={styles.textInputStyle}
                                itemStyle={styles.itemStyle}
                                itemTextStyle={{ color: '#555555' }}
                                itemsContainerStyle={{ maxHeight: 250, maxWidth: '100%' }}
                                items={this.state.items}
                                placeholder="Search here..."
                                resetValue={false}
                                underlineColorAndroid="transparent"
                            />
                            <TouchableOpacity
                                style={styles.btnBack}
                                onPress={()=>Actions.pop()}
                            >
                                <Image source={require('../res/img/back_black.png')} style={styles.backStyle} />
                            </TouchableOpacity>
                            <Image source={require('../res/img/search.png')} style={styles.searchIcon} />

                    </View>
                    </Card>
                </View>
            </Content>
        </KeyboardAvoidingView>
    </SafeAreaView>
    
    );
  }
}

const mapStateToProps = (state) => {
    return {
        calls: state.callList.calls,
    }
}
export default connect(mapStateToProps, {getInitailCall,gotoHistory})(Search);

const styles = StyleSheet.create({
    name:{
        fontSize: 14,
    },
    info:{
        marginTop: 5,
        flex:1,
        flexDirection: 'row',
    },
    duration:{
        color:'gray',
        fontSize:12,
        marginLeft:5,
    },
    content:{
        marginLeft:15,
        marginTop:10,
        flex:1,
        justifyContent: 'center',
    },
    container:{
        flex:1,
        backgroundColor:'#eee',
        flexDirection: 'column',
    },
    input:{
        flex:1,
        backgroundColor:'white',
        color:'black',
        paddingHorizontal: 15,
    },
    searchBox:{
        flex:1,
        marginTop:-5
    },
    box:{
        flex:1,
        flexDirection:'row',
        position:'relative'
    },
    searchIcon:{
        width:20,
        height:20,
        alignSelf: 'center',
        marginRight: 15,
        position:'absolute',
        top:15,
        right:5,
    },
    backStyle:{
        width:45,
        height:45,
        alignSelf: 'center',
    },
    btnBack:{
        position:'absolute',
        top:5,
        left:3,
    },  
    itemStyle:{
        padding: 12,
        marginTop: 0,
        color: 'white',
        backgroundColor: 'rgba(0,0,0,0)',
        borderTopColor: 'rgba(0,0,0,0.1)',
        borderTopWidth: 1,
        width:'100%',
        alignSelf: 'center'
    },
    textInputStyle:{
        padding: 5,
        borderBottomWidth: 1,
        marginLeft:30,
        borderBottomColor: 'rgba(0,0,0,0.01)',
        flex:1,
        color:'#555555'
    },
    containerStyle:{ 
        padding: 5,
        backgroundColor:'white', 
        paddingLeft: 10,
        width:'100%',
        color:'white',
    }
})