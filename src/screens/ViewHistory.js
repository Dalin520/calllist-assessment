import React, { Component } from 'react';
import { View, Text ,StatusBar,Platform,Image,Alert,ScrollView,ActivityIndicator,TouchableOpacity,StyleSheet} from 'react-native';
import {Header,Card,CardItem,Container,Left,Body,Title,Right, Content} from 'native-base'
import {Actions} from 'react-native-router-flux'
import {getHistory,deleteCall} from '../actions/callAction'
import { connect } from 'react-redux';

class ViewHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
// GO BACK TO HOME SCREEN
  goBack=()=>{
    Actions.pop();
    setTimeout(() => {
    Actions.refresh({name:'home'});
    }, 10);
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (JSON.stringify(this.props.calls) !== JSON.stringify(nextProps.calls)) {
        return true
    }
    else{
        return false
    }
  }
  componentWillMount() {
      var id = this.props.navigation.state.params.id;
      this.props.getHistory(id);
  }
  componentDidMount() {
      this.props.calls = {}
      console.log('calls===',this.props.calls)
  }
  
_delete=(name,id)=>{
    Alert.alert(
        `Delete "${name}" ?`,
        'Deleting this call will also delete its data.',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => {this.props.deleteCall(id);this.props.navigation.goBack()}},
        ],
        {cancelable: false},
    );
}
  render() {
    const data = this.props.calls
    console.log('data====',data)
    return (
    <Container style={styles.container}>
        {
            Platform.OS==='android'? 
                <StatusBar backgroundColor="green" barStyle="light-content" />              
            :
            <StatusBar backgroundColor="green" barStyle="dark-content" /> 
        }
        <Header
            androidStatusBarColor="green"
            style={{backgroundColor:'green'}}
            iosBarStyle="light-content"
        >
            <Left style={{
            flex:1
            }}>
                <TouchableOpacity
                    onPress={()=>this.goBack()}
                >
                    <Image source ={require('../res/img/back.png')} style={{width:25,height:25}} />
                </TouchableOpacity>
            </Left>
            <Body style={{flex:2,justifyContent:'center'}}>
                <Title style={styles.title}>{this.props.navigation.state.params.name}</Title>
            </Body>
        </Header>
        <Content>
        { data.length? 
        (
        <View>
            <Text style={styles.txt}>You have called {data.length} {data.length>1?"times":"time"}</Text>
            {/* List History result */}
            <ScrollView showsVerticalScrollIndicator={false}>
                {data.map((item,index) => {
                    return (                    
                    <View key={index} style={{marginHorizontal:10}}>
                        <Card>
                            <CardItem>                 
                            <Image source={item.img} style={{ width: 60, height: 60 }} resizeMode="cover" />
                            <Body>
                                <View style={styles.content}>
                                    <Text style={styles.name}>{item.name} </Text>
                                    <View style={styles.info}>
                                        <Image source={require('../res/img/info.png')} style={{ width: 15, height: 15}} />
                                        <Text style={styles.duration}>{item.duration}</Text>
                                    </View>
                                </View>
                            </Body>
                            <Right>
                                <TouchableOpacity
                                    onPress={()=>this._delete(item.name,item.id)}
                                >
                                    <Image source={require('../res/img/delete.png')} style={{ width: 30, height: 30 }} />
                                </TouchableOpacity>
                            </Right>
                            </CardItem>
                        </Card> 
                    </View>
                    )
                })}
            </ScrollView>
        </View>
        )
        : <ActivityIndicator size="large" color="#0000ff" /> 
        }
        </Content>
    </Container>
    );
  }
}
const mapStateToProps = (state) => {
    return {
        calls: state.callList.histories,
    }
}
export default connect(mapStateToProps, {getHistory,deleteCall})(ViewHistory);
const styles = StyleSheet.create({
    name:{
        fontSize: 14,
    },
    info:{
        marginTop: 5,
        flex:1,
        flexDirection: 'row',
    },
    duration:{
        color:'gray',
        fontSize:12,
        marginLeft:5,
    },
    content:{
        marginLeft:15,
        marginTop:10,
        flex:1,
        justifyContent: 'center',
    },
    title:{
        justifyContent:'center',
        color:'white',
        fontSize: 18,
    },
    txt:{
        margin:5,
        marginLeft:10,
        fontSize:16,
        fontWeight: 'bold',
        color: 'green',
    },
    container:{
        backgroundColor:'#eee'
    }
})