import {jsonResponse} from '../res/jsonResponse'
import {actionType} from './actionType'

// call initail from jsonResponse
let calls = jsonResponse.data

export const  getInitailCall = () => {
  return {
      type:actionType.GET_INITIAL_CALL,
      payload:calls
  }
}

export const deleteCall = (id,duration) => {
  // remove specific index if there is duplicate item in arr
  var data = calls
  const filteredItems = data.filter(item=> item.id !== id )
  calls = filteredItems
  return {
      type:actionType.DELETE,
      payload:filteredItems
  }
}

export const getHistory = (id) =>{
  let values = calls;
  let res = values.filter(x => x.id === id)
  return {
      type:actionType.GET_HISTORY,
      payload:res
  };
}