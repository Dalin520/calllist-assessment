import {actionType} from '../actions/actionType'
const initState={
    calls: [],
    histories:[],
}

export const callReducer = (state = initState, action) => {
    switch (action.type) {
        case actionType.GET_INITIAL_CALL:
            return { ...state, calls: action.payload }    
        case actionType.DELETE:
            return { ...state, calls:action.payload}
        case actionType.GET_HISTORY:
            return { ...state, histories:action.payload}
        default:
            return state
    }
}
