/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Provider } from 'react-redux';
import {CallCentralStore} from './src/stores/CallCentralStore'
import Main from './src/screens/main/Main';

export default class App extends Component {
  render() {
    return (
      <Provider store={CallCentralStore}>
        <Main/>
      </Provider>
    );
  }
}
